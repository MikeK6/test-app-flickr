//
//  FirstTableViewController.swift
//  Flickr Public Feed
//
//  Created by Mikhail Kolesov on 21.03.2023.
//

import UIKit

class FirstTableViewController: UITableViewController {
    
    private var originalPhotos: [Item] = []
    private var photos: [Item] = []
    private let networkManager = NetworkManager()
    
    @IBOutlet private var searchBar: UISearchBar!
    
    @IBAction private func sorted(_ sender: Any) {
        createAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        fetchData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showDetailView" else {
            return
        }
        if let secondVC = segue.destination as? SecondViewController, let indexPath = tableView.indexPathForSelectedRow {
            let index = indexPath.row
            let urlString = photos[index].media.m
            secondVC.imageUrlString = urlString
        }
    }
    
    private func fetchData(with tags: String = "") {
        networkManager.fetchData(for: tags, completion: { [weak self] photos in
            self?.originalPhotos = photos
            self?.photos = photos
            self?.tableView.reloadData()
        })
    }
    
    func createAlert() {
        let alert = UIAlertController(title: "", message: "Выберете параметр сортировки", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Название", style: .default) { _ in
            self.sortedPhotoforTitle()
        })
        alert.addAction(UIAlertAction(title: "Дата создания", style: .default) { _ in
            self.sortedPhotoforDateCreate()
        })
        alert.addAction(UIAlertAction(title: "Отменить", style: .default) { _ in
            self.cancelSortedPhoto()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    private func sortedPhotoforTitle() {
        let sortedPhotos = originalPhotos.sorted {$0.title < $1.title}
        self.photos = sortedPhotos
        tableView.reloadData()
    }
    
    private func sortedPhotoforDateCreate() {
        let sortedPhotos = originalPhotos.sorted {$0.dateTaken < $1.dateTaken}
        self.photos = sortedPhotos
        tableView.reloadData()
    }
    
    private func cancelSortedPhoto() {
        self.photos = originalPhotos
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell
        else {
            return UITableViewCell()
        }
        cell.configure(with: photos[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension FirstTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        fetchData(with: searchText)
    }
}
