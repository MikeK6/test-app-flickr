//
//  SecondViewController.swift
//  Flickr Public Feed
//
//  Created by Mikhail Kolesov on 23.03.2023.
//

import UIKit
import Kingfisher

class SecondViewController: UIViewController {
    
    public var imageUrlString = ""
    
    @IBOutlet private var detailedImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setImage()
    }
    
    private func setImage() {
        if let url = URL(string: imageUrlString) {
            detailedImageView.kf.setImage(with: url)
        }
    }
}
