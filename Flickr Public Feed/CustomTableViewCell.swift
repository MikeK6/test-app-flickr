//
//  CustomTableViewCell.swift
//  Flickr Public Feed
//
//  Created by Mikhail Kolesov on 21.03.2023.
//

import UIKit
import Kingfisher

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet private var imagePicture: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var dateOfCreateLabel: UILabel!
    @IBOutlet private var tagsLabel: UILabel!
    
    public func configure(with photo: Item) {
        nameLabel.text = photo.title
        dateOfCreateLabel.text = dateConvert(date: photo.dateTaken)
        tagsLabel.text = photo.tags
        if let url = URL(string: photo.media.m) {
            imagePicture.kf.setImage(with: url)
        }
    }
    
    private func dateConvert(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let convert = dateFormatter.date(from: date) else { return "Value is not converted to date" }
        dateFormatter.dateFormat = "MMM d, yyyy"
        let convert2 = dateFormatter.string(from: convert)
        return convert2
    }
}
