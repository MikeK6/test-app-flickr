//
//  NetworkManager.swift
//  Flickr Public Feed
//
//  Created by Mikhail Kolesov on 21.03.2023.
//

import Foundation

struct NetworkManager {
    func fetchData(for tags: String, completion: ((_ photos: [Item]) -> Void)? = nil) {
        guard let url = URL(string: "https://www.flickr.com/services/feeds/photos_public.gne?format=json&tags=\(tags)&nojsoncallback=1") else { return }
        let session = URLSession.init(configuration: .default)
        let task = session.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            parseJSON(withData: data) { photos in
                completion?(photos)
            }
        }
        task.resume()
    }
    
    func parseJSON(withData data: Data, completion: ((_ photos: [Item]) -> Void)? = nil) {
        let decoder = JSONDecoder()
        do {
            let rawResponse = try decoder.decode(RawResponse.self, from: data)
            DispatchQueue.main.async {
                completion?(rawResponse.items)
            }
        } catch let error {
            print(error)
        }
    }
}
